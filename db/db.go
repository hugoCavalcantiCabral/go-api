package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"go-api/models"
	"log"
	"os"
)

var DB *gorm.DB

func Open(teste bool, envFile string) {

	//path := os.Getenv("GOPATH")
	//e := godotenv.Load(path + "/src/go-api/.env")
	e := godotenv.Load(envFile)
	if e != nil {
		log.Fatal(e)
	}

	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	if teste {
		dbName = os.Getenv("db_name_test")
	}
	dbHost := os.Getenv("db_host")

	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)
	if dbName == os.Getenv("db_name") {
		fmt.Println(dbUri)
	}

	var err error
	conn, err := gorm.Open("postgres", dbUri)
	if err != nil {
		log.Fatal(err)
	}

	DB = conn
	inicializaDadosDB()
}

func inicializaDadosDB(){
	DB.Debug().AutoMigrate(&models.Account{}, &models.OperationType{}, &models.Transaction{})

	rows := 0
	DB.Model(&models.OperationType{}).Count(&rows)

	if rows == 0 {
		operationCompraAVista := models.OperationType{Description: "COMPRA A VISTA", ChargeOrder: 2}
		operationCompraParcelada := models.OperationType{Description: "COMPRA PARCELADA", ChargeOrder: 1}
		operationSaque := models.OperationType{Description: "SAQUE", ChargeOrder: 0}
		operationPagamento := models.OperationType{Description: "PAGAMENTO", ChargeOrder: 0}

		DB.Model(&models.OperationType{}).Create(&operationCompraAVista)
		DB.Model(&models.OperationType{}).Create(&operationCompraParcelada)
		DB.Model(&models.OperationType{}).Create(&operationSaque)
		DB.Model(&models.OperationType{}).Create(&operationPagamento)
	}
}

func Close() error {
	return DB.Close()
}
