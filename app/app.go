package app

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
	"go-api/controllers"
	"log"
	"net/http"
	"os"
)

type App struct {
	Router *mux.Router
}

func (a *App) Initialize() {
	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) Run() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	fmt.Println("Serviço rodando na porta:" + port)

	log.Fatal(http.ListenAndServe(":8000", a.Router))
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/v1/accounts/limits", controllers.Limits).Methods("GET")
	a.Router.HandleFunc("/v1/accounts/limits/{id}", controllers.GetAccount).Methods("GET")
	a.Router.HandleFunc("/v1/accounts/{id}", controllers.ReCalcAvailableResources).Methods("PATCH")
	a.Router.HandleFunc("/v1/transactions", controllers.Transaction).Methods("POST")
	a.Router.HandleFunc("/v1/payments", controllers.Payments).Methods("POST")
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}