package tests

import (
	"go-api/db"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestRouter(t *testing.T) {

	InstanciarRouter("../.env")

	//mockando serve para testes
	mockServer := httptest.NewServer(A.Router)

	//Testando rotas
	resp, err := http.Get(mockServer.URL + "/v1/accounts/limits")

	if err != nil {
		t.Fatal(err)
	}

	CheckResponseCode(t, http.StatusOK, resp.StatusCode)

	defer db.Close()
}

func TestRouterForNonExistentRoute(t *testing.T) {

	InstanciarRouter("../.env")

	mockServer := httptest.NewServer(A.Router)
	//testando rota inexistente
	resp, err := http.Post(mockServer.URL+"/v1/accounts/limitss", "", nil)

	if err != nil {
		t.Fatal(err)
	}

	// Como a rota não existe então é esperado o erro
	CheckResponseCode(t, http.StatusMethodNotAllowed , resp.StatusCode)

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	respString := strings.TrimRight(string(b), "\r\n")
	expected := ""

	CheckResponse(t, expected, respString)

	defer db.Close()
}