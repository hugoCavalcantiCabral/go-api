package controllers

import (
	"bytes"
	"encoding/json"
	"go-api/db"
	"go-api/models"
	"go-api/tests"
	"go-api/tests/generator"
	"net/http"
	"testing"
	"time"
)

// Não são aceitos pagamentos com valores negativos
func TestGetInvalidPayment(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	generator.GenerateAccounts(1);

	values := []map[string]interface{}{
		{"account_id": 1,
		"amount": -1}}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/payments", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "Error, this kind of operation doesn't not allow negative amounts", m["message"])
}

func TestLowPayment(t *testing.T){
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()

	account := models.Account{AvailableWithdrawalLimit:100, AvailableCreditLimit:100}
	db.DB.Save(&account)

	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "SAQUE").First(&operation).Error
	if err != nil{
		t.Errorf(err.Error())
		return
	}

	amount := -10.0
	transction := models.Transaction{AccountId:int64(account.ID), OperationTypeId:int64(operation.ID), EventDate:time.Now(),
		DueDate:time.Now(), Amount: amount, Balance:amount}
	db.DB.Save(&transction)

	pay1 := 3.0
	pay2 := 4.0
	values := []map[string]interface{}{
		{"account_id": account.ID,
		"amount": pay1},
		{"account_id": account.ID,
			"amount": pay2}}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/payments", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)
	var m map[string]interface{}
	err = json.Unmarshal(response.Body.Bytes(), &m)
	if err != nil{
		t.Errorf(err.Error())
	}
	tests.CheckResponse(t, "success", m["message"].(string))

	if val, ok := m["payment"]; ok {
		v := val.([]interface{})

		for _, it := range v{
			payBalance := it.(map[string]interface{})["balance"].(float64)
			if payBalance != 0.0 {
				t.Errorf("Expected balance of payment %f. Got %f\n",
					0.0, payBalance)
			}
		}
	}

	transaction2 := models.Transaction{}
	err = db.DB.Table("transactions").Where("id = ? ", transction.ID).First(&transaction2).Error
	account2 := models.Account{}
	err2 := db.DB.Table("accounts").Where("id = ? ", account.ID).First(&account2).Error

	if err != nil || err2 != nil{
		if err != nil {
			t.Errorf(err.Error())
		}else {
			t.Errorf(err2.Error())
		}
		return
	}

	if account.AvailableWithdrawalLimit + pay2 + pay1 != account2.AvailableWithdrawalLimit{
		t.Errorf("Expected available withdraw limit %f. Got %f\n",
			account.AvailableWithdrawalLimit + pay2 + pay1, account2.AvailableWithdrawalLimit)
	}
	balance := amount + pay2 + pay1
	if transaction2.Balance != balance {
		t.Errorf("Expected balance of transaction %f. Got %f\n",
			balance, transaction2.Balance)
	}
}

func TestHighPayment(t *testing.T){
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()

	account := models.Account{AvailableWithdrawalLimit:100, AvailableCreditLimit:100}
	db.DB.Save(&account)

	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "SAQUE").First(&operation).Error
	if err != nil{
		t.Errorf(err.Error())
		return
	}

	amount := -10.0
	transction := models.Transaction{AccountId:int64(account.ID), OperationTypeId:int64(operation.ID), EventDate:time.Now(),
		DueDate:time.Now(), Amount: amount, Balance: amount}
	db.DB.Save(&transction)

	pay := 13.0
	values := []map[string]interface{}{
		{"account_id": account.ID,
			"amount": pay}}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/payments", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)
	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "success", m["message"].(string))

	balanceLeft := pay + amount
	if val, ok := m["payment"]; ok {
		it := val.([]interface{})
		payBalance := it[0].(map[string]interface{})["balance"]
		if payBalance != balanceLeft {
			t.Errorf("Expected balance of payment %f. Got %f\n",
				balanceLeft, payBalance)
		}
	}

	transaction2 := models.Transaction{}
	err = db.DB.Table("transactions").Where("id = ? ", transction.ID).First(&transaction2).Error
	account2 := models.Account{}
	err2 := db.DB.Table("accounts").Where("id = ? ", account.ID).First(&account2).Error

	if err != nil || err2 != nil{
		if err != nil {
			t.Errorf(err.Error())
		}else {
			t.Errorf(err2.Error())
		}
		return
	}

	if account.AvailableWithdrawalLimit != account2.AvailableWithdrawalLimit + amount{
		t.Errorf("Expected available withdraw limit %f. Got %f\n",
			account.AvailableWithdrawalLimit - amount, account2.AvailableWithdrawalLimit)
	}
	if transaction2.Balance != 0.0 {
		t.Errorf("Expected balance of transaction %f. Got %f\n",
			0.0, transaction2.Balance)
	}
}

func TestOrderPayment(t *testing.T){

	tests.InstanciarRouter("../../.env")
	tests.ClearTable()

	account := models.Account{AvailableWithdrawalLimit:100, AvailableCreditLimit:100}
	db.DB.Save(&account)

	compraParcelada := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "COMPRA PARCELADA").First(&compraParcelada).Error
	if err != nil{
		t.Errorf(err.Error())
		return
	}
	amount := -10.0
	transctionCompra := models.Transaction{AccountId:int64(account.ID), OperationTypeId:int64(compraParcelada.ID), EventDate:time.Now(),
		DueDate:time.Now(), Amount: amount, Balance: amount}
	db.DB.Save(&transctionCompra)

	time.Sleep(1 * time.Second)

	saque := models.OperationType{}
	err = db.DB.Table("operation_types").Where("description = ? ", "SAQUE").First(&saque).Error
	if err != nil{
		t.Errorf(err.Error())
		return
	}
	transctionSaque := models.Transaction{AccountId:int64(account.ID), OperationTypeId:int64(saque.ID), EventDate:time.Now(),
		DueDate:time.Now(), Amount: amount, Balance: amount}
	db.DB.Save(&transctionSaque)

	pay := 15.0
	values := []map[string]interface{}{
		{"account_id": account.ID,
		"amount": pay}}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/payments", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)
	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "success", m["message"].(string))

	balanceLeft := 0.0
	if val, ok := m["payment"]; ok {
		it := val.([]interface{})
		payBalance := it[0].(map[string]interface{})["balance"]
		if payBalance != balanceLeft {
			t.Errorf("Expected balance of payment %f. Got %f\n",
				balanceLeft, payBalance)
		}
	}

	transactionSaque2 := models.Transaction{}
	err = db.DB.Table("transactions").Where("id = ? ", transctionSaque.ID).First(&transactionSaque2).Error
	transactionCompra2 := models.Transaction{}
	err3 := db.DB.Table("transactions").Where("id = ? ", transctionCompra.ID).First(&transactionCompra2).Error
	account2 := models.Account{}
	err2 := db.DB.Table("accounts").Where("id = ? ", account.ID).First(&account2).Error

	if err != nil || err2 != nil || err3 != nil{
		if err != nil {
			t.Errorf(err.Error())
		}else if err2 != nil{
			t.Errorf(err2.Error())
		}else {
			t.Errorf(err3.Error())
		}
		return
	}

	accountBalanceWithDraw := account.AvailableWithdrawalLimit - amount
	accountBalanceCredit := account.AvailableCreditLimit + 5.0
	if accountBalanceWithDraw != account2.AvailableWithdrawalLimit {
		t.Errorf("Expected available withdraw limit %f. Got %f\n",
			accountBalanceWithDraw , account2.AvailableWithdrawalLimit)
	}
	if accountBalanceCredit != account2.AvailableCreditLimit {
		t.Errorf("Expected available credit limit %f. Got %f\n",
			accountBalanceCredit, account2.AvailableCreditLimit)
	}
	if transactionSaque2.Balance != 0.0 {
		t.Errorf("Expected balance of transaction %f. Got %f\n",
			0.0, transactionSaque2.Balance)
	}
	if transactionCompra2.Balance != -5.0 {
		t.Errorf("Expected balance of transaction %f. Got %f\n",
			-5.0, transactionCompra2.Balance)
	}
}