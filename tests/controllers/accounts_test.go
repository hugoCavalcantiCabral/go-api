package controllers

import (
	"bytes"
	"encoding/json"
	"go-api/db"
	"go-api/models"
	"go-api/tests"
	"net/http"
	"testing"
	g "go-api/tests/generator"
)

func TestGetAccount(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()

	account := models.Account{AvailableWithdrawalLimit:100.5, AvailableCreditLimit:10.3}
	db.DB.Save(&account)

	req, _ := http.NewRequest("GET", "/v1/accounts/limits/1", nil)
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	accountDB := models.Account{}
	tests.Decode("accounts", response.Body.Bytes(), &accountDB)

	if accountDB.ID == 0{
		t.Errorf("Error trying to get an account")
	}
	if accountDB.AvailableCreditLimit != account.AvailableCreditLimit{
		t.Errorf("Expected available credit limit %f. Got %f\n", account.AvailableCreditLimit, accountDB.AvailableCreditLimit)
	}
	if accountDB.AvailableWithdrawalLimit != account.AvailableWithdrawalLimit{
		t.Errorf("Expected available withdraw limit %f. Got %f\n",
			account.AvailableWithdrawalLimit, accountDB.AvailableWithdrawalLimit)
	}
	tests.CloseDB()
}

func TestGetNonExistentAccount(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()

	req, _ := http.NewRequest("GET", "/v1/accounts/limits/11", nil)
	response := tests.ExecuteRequest(req)

	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "record not found", m["message"])

	tests.CloseDB()
}

func TestChangeAvailableResources(t *testing.T){
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	g.GenerateAccounts(1);

	account := models.Account{}
	req, _ := http.NewRequest("GET", "/v1/accounts/limits/1", nil)
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)
	tests.Decode("accounts", response.Body.Bytes(), &account)

	availableCredit := account.AvailableCreditLimit
	availableWithdraw := account.AvailableWithdrawalLimit
	changeCredit := 10.0
	changeWithdraw := 50.0

	values := map[string]map[string]float64{
		"available_credit_limit": map[string]float64{"amount": changeCredit},
		"available_withdrawal_limit": map[string]float64{"amount": changeWithdraw}}
	jsonValue, _ := json.Marshal(values)

	reqSave, _ := http.NewRequest("PATCH", "/v1/accounts/1", bytes.NewBuffer(jsonValue))
	responseSave := tests.ExecuteRequest(reqSave)
	tests.CheckResponseCode(t, http.StatusOK, responseSave.Code)

	response = tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)
	tests.Decode("accounts", response.Body.Bytes(), &account)

	if account.AvailableCreditLimit != availableCredit + changeCredit {
		t.Errorf("Expected available credit limit %f. Got %f\n", availableCredit + changeCredit, account.AvailableCreditLimit)
	}
	if account.AvailableWithdrawalLimit != availableWithdraw + changeWithdraw {
		t.Errorf("Expected available credit limit %f. Got %f\n",
			availableWithdraw + changeWithdraw, account.AvailableWithdrawalLimit)
	}
}

func TestChangeAvailableResourcesToNegative(t *testing.T){
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	g.GenerateAccounts(1);

	account := models.Account{}
	req, _ := http.NewRequest("GET", "/v1/accounts/limits/1", nil)
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)
	tests.Decode("accounts", response.Body.Bytes(), &account)

	availableCredit := account.AvailableCreditLimit
	availableWithdraw := account.AvailableWithdrawalLimit
	changeCredit := (-1 * availableCredit) - 10.0
	changeWithdraw := (-1 *availableWithdraw ) - 50.0

	values := map[string]map[string]float64{
		"available_credit_limit": map[string]float64{"amount": changeCredit},
		"available_withdrawal_limit": map[string]float64{"amount": changeWithdraw}}
	jsonValue, _ := json.Marshal(values)

	reqSave, _ := http.NewRequest("PATCH", "/v1/accounts/1", bytes.NewBuffer(jsonValue))
	responseSave := tests.ExecuteRequest(reqSave)
	tests.CheckResponseCode(t, http.StatusOK, responseSave.Code)

	var f interface{}
	err := json.Unmarshal(responseSave.Body.Bytes(), &f)
	if err != nil {
		t.Errorf(err.Error())
		return
	}

	mapa := f.(map[string]interface{})
	val := mapa["message"]
	tests.CheckResponse(t, "Reject" , val.(string))
}
