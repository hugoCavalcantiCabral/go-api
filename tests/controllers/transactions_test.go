package controllers

import (
	"bytes"
	"encoding/json"
	"go-api/db"
	"go-api/models"
	"go-api/tests"
	"go-api/tests/generator"
	"net/http"
	"testing"
)

func TestGetInvalidTransaction(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	generator.GenerateAccounts(1);

	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "PAGAMENTO").First(&operation).Error

	if err != nil{
		t.Errorf(err.Error())
		return
	}

	values := map[string]interface{}{
		"account_id": 1,
		"operation_type_id": operation.ID,
		"amount": -10.0}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/transactions", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "Operation not allowed", m["message"])
}

func TestGetInvalidTransaction2(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	generator.GenerateAccounts(1);

	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "SAQUE").First(&operation).Error

	if err != nil{
		t.Errorf(err.Error())
		return
	}

	values := map[string]interface{}{
		"account_id": 1,
		"operation_type_id": operation.ID,
		"amount": 10.0}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/transactions", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "Error, this kind of operation doesn't not allow positive amounts", m["message"])
}

func TestLimitWithdraw(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	account := models.Account{AvailableWithdrawalLimit:100.5, AvailableCreditLimit:10.3}
	db.DB.Save(&account)

	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "SAQUE").First(&operation).Error

	if err != nil{
		t.Errorf(err.Error())
		return
	}

	values := map[string]interface{}{
		"account_id": account.ID,
		"operation_type_id": operation.ID,
		"amount": (account.AvailableWithdrawalLimit + 0.1)*-1}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/transactions", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "Reject", m["message"])

	accountNew := models.Account{}
	err = db.DB.Table("accounts").Where(" id = ? ", account.ID).First(&accountNew).Error
	if err != nil{
		t.Errorf(err.Error())
		return
	}

	if accountNew.AvailableWithdrawalLimit != account.AvailableWithdrawalLimit{
		t.Errorf("Expected available withdraw limit %f. Got %f\n",
			account.AvailableWithdrawalLimit, accountNew.AvailableWithdrawalLimit)
	}
}

func TestLimitCredit(t *testing.T) {
	tests.InstanciarRouter("../../.env")
	tests.ClearTable()
	account := models.Account{AvailableWithdrawalLimit:100.5, AvailableCreditLimit:10.3}
	db.DB.Save(&account)

	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "COMPRA PARCELADA").First(&operation).Error

	if err != nil{
		t.Errorf(err.Error())
		return
	}

	values := map[string]interface{}{
		"account_id": account.ID,
		"operation_type_id": operation.ID,
		"amount": (account.AvailableCreditLimit + 0.1 ) * -1}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", "/v1/transactions", bytes.NewBuffer(jsonValue))
	response := tests.ExecuteRequest(req)
	tests.CheckResponseCode(t, http.StatusOK, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	tests.CheckResponse(t, "Reject", m["message"])

	accountNew := models.Account{}
	err = db.DB.Table("accounts").Where(" id = ? ", account.ID).First(&accountNew).Error
	if err != nil{
		t.Errorf(err.Error())
		return
	}

	if accountNew.AvailableCreditLimit != account.AvailableCreditLimit{
		t.Errorf("Expected available credit limit %f. Got %f\n",
			account.AvailableCreditLimit, accountNew.AvailableCreditLimit)
	}
}