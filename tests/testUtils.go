package tests

import (
	"encoding/json"
	"go-api/app"
	"go-api/db"
	"go-api/models"
	"net/http"
	"net/http/httptest"
	"testing"
)

var A app.App

func InstanciarRouter(envFile string){
	//instanciando o router
	A = app.App{}
	A.Initialize()
	OpenDB(envFile)
}

func OpenDB(envFile string){
	db.Open(true, envFile)
}

func CloseDB(){
	defer db.Close()
}

func ClearTable() {
	db.DB.Exec("DELETE FROM transactions CASCADE")
	db.DB.Exec("ALTER SEQUENCE transactions_id_seq RESTART WITH 1")

	db.DB.Exec("DELETE FROM accounts CASCADE")
	db.DB.Exec("ALTER SEQUENCE accounts_id_seq RESTART WITH 1")
}

func CheckResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func CheckResponse(t *testing.T, expected, actual string) {
	if expected != actual {
		t.Errorf("Expected response %s. Got %s\n", expected, actual)
	}
}

func ExecuteRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	A.Router.ServeHTTP(rr, req)

	return rr
}

func Decode(data string, response []byte, account *models.Account) {
	var f interface{}
	json.Unmarshal(response, &f)
	mapa := f.(map[string]interface{})
	if val, ok := mapa[data]; ok {
		j, _ := json.Marshal(val)
		json.Unmarshal(j, account)
	}
}
