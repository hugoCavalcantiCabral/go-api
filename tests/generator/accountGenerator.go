package generator

import (
	fake "github.com/brianvoe/gofakeit"
	"go-api/db"
	"go-api/models"
	"time"
)

func GenerateAccounts(cont int){
	fake.Seed(time.Now().UnixNano())

	for i := 0 ; i < cont ; i++{
		account := models.Account{}

		account.AvailableWithdrawalLimit = fake.Float64Range(0.0, 1500.0)
		account.AvailableCreditLimit = fake.Float64Range(0.0, 1500.0)

		db.DB.Save(&account)
	}
}