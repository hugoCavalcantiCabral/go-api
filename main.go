package main

import (
	"go-api/app"
	"go-api/db"
)

func main() {

	db.Open(false, ".env")

	a := app.App{}

	a.Initialize()

	a.Run()

	defer db.Close()
}
