package controllers

import (
	"encoding/json"
	"fmt"
	"go-api/db"
	"go-api/models"
	"go-api/utils"
	"io/ioutil"
	"math"
	"net/http"
	"time"
)

var debitTypes = []string{"COMPRA A VISTA", "COMPRA PARCELADA", "SAQUE"}

var Transaction = func(w http.ResponseWriter, r *http.Request) {
	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		utils.Respond(w, utils.Message(false, readErr.Error()))
		return
	}
	requestBody := []byte(body)

	idAccount, idOperation, amount, e := getDataFromTransaction(requestBody, w)
	if !e {
		return
	}
	if amount >= 0{
		utils.Respond(w, utils.Message(false, "Error, this kind of operation doesn't not allow positive amounts"))
		return
	}

	//checar se há saldo positivo, se houver então já abater do amount, e só descontar no cliente o saldo real consumido
	//se houver saldo positivo também abater o próprio saldo positivo ao final da operação
	positiveBalance, er := getPositiveBalance(idAccount)
	if er != nil {
		utils.Respond(w, utils.Message(false, "Error, contact the system administrator"))
		fmt.Printf(er.Error())
		return
	}

	minimum := math.Min(positiveBalance, amount*-1)
	balance := amount + minimum
	positiveBalanceConsume := minimum

	if !validateTransaction(idAccount, idOperation, balance, w) {
		return
	}

	transaction := models.Transaction{Balance:balance, AccountId:int64(idAccount), Amount:amount, DueDate:time.Now(),
		EventDate:time.Now(), OperationTypeId:int64(idOperation)}

	db.DB.Save(&transaction)

	if balance < 0{
		err := updateClient(balance, idAccount, idOperation)

		if err != nil {
			utils.Respond(w, utils.Message(false, "Error, contact the system administrator"))
			fmt.Printf(er.Error())
			return
		}
	}

	if positiveBalanceConsume > 0{
		err := updatePositiveBalance(positiveBalanceConsume, idAccount)

		if err != nil {
			utils.Respond(w, utils.Message(false, "Error, contact the system administrator"))
			fmt.Printf(er.Error())
			return
		}
	}

	resp := utils.Message(true, "success")
	resp["transaction"] = transaction

	utils.Respond(w, resp)
}

func updateClient(balance float64, idAccount int, idOperation int) error{
	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("id = ? ", idOperation).First(&operation).Error
	if err != nil {
		return err
	}

	account := models.Account{}
	err = db.DB.Table("accounts").Where("id = ? ", idAccount).First(&account).Error
	if err != nil {
		return err
	}

	if operation.Description == debitTypes[1]{
		account.AvailableCreditLimit += balance
	}else{
		account.AvailableWithdrawalLimit += balance
	}

	db.DB.Save(&account)

	return nil
}

func updatePositiveBalance(balance float64, idAccount int) error {
	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "PAGAMENTO").First(&operation).Error
	if err != nil {
		return err
	}

	var transactions []models.Transaction
	err = db.DB.Table("transactions").Where("account_id = ? AND operation_type_id = ? and balance > 0.0", idAccount, operation.ID).
		Order("event_date ASC ").Find(&transactions).Error
	if err != nil{
		return err
	}

	for i := 0 ; i < len(transactions) && balance > 0 ; i++ {
		temp := transactions[i]

		minimum := math.Min(balance, temp.Balance)

		temp.Balance -= minimum
		balance -= minimum

		db.DB.Save(&temp)
	}

	return nil
}

func getPositiveBalance(idAccount int) (float64, error) {
	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "PAGAMENTO").First(&operation).Error
	if err != nil {
		return 0, err
	}

	row := db.DB.Table("transactions").Select(" COALESCE(SUM(balance), 0) ").
		Where("account_id = ? AND operation_type_id = ?", idAccount, operation.ID).Row()

	if row == nil{
		return 0, nil
	}
	var val float64
	err = row.Scan(&val)
	if err != nil{
		return 0, err
	}

	return val, nil
}

func validateTransaction(idAccount int, idOperation int, amount float64, w http.ResponseWriter) bool {
	account, e := getAccount(idAccount, w)
	if !e {
		return e
	}

	operation, e := getOperation(idOperation, w)
	if !e {
		return e
	}

	if !debitTypesOperation(operation){
		utils.Respond(w, utils.Message(false, "Operation not allowed"))
		return false
	}

	if operation.Description == debitTypes[1]{
		if account.AvailableCreditLimit + amount < 0.0 {
			utils.Respond(w, utils.Message(false, "Reject"))
			return false
		}
	}else{
		if account.AvailableWithdrawalLimit + amount < 0.0 {
			utils.Respond(w, utils.Message(false, "Reject"))
			return false
		}
	}

	return true
}

func debitTypesOperation(operationType models.OperationType) bool {
	for _, ot := range debitTypes {
		if ot == operationType.Description {
			return true
		}
	}
	return false
}

func getOperation(idOperation int, w http.ResponseWriter) (models.OperationType, bool){
	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("id = ?", idOperation).Find(&operation).Error
	if err != nil {
		fmt.Println(err)
		utils.Respond(w, utils.Message(false, err.Error()))
		return operation, false
	}
	if operation == (models.OperationType{}) {
		resp := utils.Message(true, "success")
		resp["operation"] = "Operation not found"
		utils.Respond(w, resp)
		return operation, false
	}
	return operation, true
}

func getDataFromTransaction(requestBody []byte, w http.ResponseWriter) (int, int, float64, bool) {
	var f interface{}
	err := json.Unmarshal(requestBody, &f)

	idAccount := 0
	idOperation := 0
	amount := 0.0

	if err != nil {
		utils.Respond(w, utils.Message(false, err.Error()))
		return idAccount, idOperation, amount, false
	}

	mapa := f.(map[string]interface{})
	if val, ok := mapa["account_id"]; ok {
		idAccount = int(val.(float64))
	}
	if val, ok := mapa["operation_type_id"]; ok{
		idOperation = int(val.(float64))
	}
	if val, ok := mapa["amount"]; ok{
		amount = val.(float64)
	}

	return idAccount, idOperation, amount, true
}