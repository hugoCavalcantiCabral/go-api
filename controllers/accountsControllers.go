package controllers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"go-api/db"
	"go-api/models"
	u "go-api/utils"
	"io/ioutil"
	"net/http"
	"strconv"
)

var accountNotFound = "Account not found"

var Limits = func(w http.ResponseWriter, r *http.Request) {
	accounts := make([]*models.Account, 0)
	err := db.DB.Table("accounts").Find(&accounts).Error
	if err != nil {
		u.Respond(w, u.Message(false, accountNotFound))
		return
	}

	resp := u.Message(true, "success")

	if len(accounts) == 0 {
		resp["accounts"] = accountNotFound
	}else {
		resp["accounts"] = accounts
	}

	u.Respond(w, resp)
}

var ReCalcAvailableResources = func(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	uid := params["id"]
	id, _ := strconv.Atoi(uid)

	account, e := getAccount(id, w)
	if !e {
		return
	}

	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		u.Respond(w, u.Message(false, readErr.Error()))
		return
	}

	requestBody := []byte(body)
	account, e = calculateNewAvailableResources(account, requestBody, w)

	if !e {
		return
	}

	db.DB.Save(&account)

	resp := u.Message(true, "success")
	resp["accounts"] = account
	u.Respond(w, resp)
}

func calculateNewAvailableResources(account models.Account, requestBody []byte,  w http.ResponseWriter) (models.Account, bool){
	var f interface{}
	err := json.Unmarshal(requestBody, &f)

	if err != nil {
		u.Respond(w, u.Message(false, err.Error()))
		return account, false
	}

	mapa := f.(map[string]interface{})
	if val, ok := mapa["available_credit_limit"]; ok {
		v := val.(map[string]interface{})
		account.AvailableCreditLimit += v["amount"].(float64)
	}
	if val, ok := mapa["available_withdrawal_limit"]; ok {
		v := val.(map[string]interface{})
		account.AvailableWithdrawalLimit += v["amount"].(float64)
	}

	if account.AvailableCreditLimit < 0.0 || account.AvailableWithdrawalLimit < 0.0{
		u.Respond(w, u.Message(false, "Reject"))
		return account, false
	}

	return account, true
}

func getAccount(id int, w http.ResponseWriter) (models.Account, bool){
	account := models.Account{}
	err := db.DB.Table("accounts").Where("id = ?", id).Find(&account).Error
	if err != nil {
		u.Respond(w, u.Message(false, err.Error()))
		return account, false
	}
	if account == (models.Account{}) {
		u.Respond(w, u.Message(false, accountNotFound))
		return account, false
	}
	return account, true
}

var GetAccount = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	uid := params["id"]

	id, _ := strconv.Atoi(uid)

	account, e := getAccount(id, w)
	if !e {
		return
	}

	resp := u.Message(true, "success")

	resp["accounts"] = account

	u.Respond(w, resp)
}