package controllers

import (
	"encoding/json"
	"fmt"
	"go-api/db"
	"go-api/models"
	"go-api/utils"
	"io/ioutil"
	"math"
	"net/http"
	"time"
)

var Payments = func(w http.ResponseWriter, r *http.Request) {
	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		utils.Respond(w, utils.Message(false, readErr.Error()))
		return
	}
	requestBody := []byte(body)

	var payments []models.Payment
	err := json.Unmarshal(requestBody, &payments)
	if err != nil {
		utils.Respond(w, utils.Message(false,  err.Error()))
		return
	}

	if !validatePayment(payments){
		utils.Respond(w, utils.Message(false, "Error, this kind of operation doesn't not allow negative amounts"))
		return
	}

	var transactions = []models.Transaction{}
	transactions, err = generatePayments(payments, w)

	if err != nil{
		return
	}

	resp := utils.Message(true, "success")
	resp["payment"] = transactions

	utils.Respond(w, resp)
}

func validatePayment(payments []models.Payment) bool {
	for _, pay := range payments{
		if pay.Amount < 0.0{
			return false
		}
	}
	return true
}

func generatePayments(payments []models.Payment, w http.ResponseWriter) ([]models.Transaction, error){

	var retorno = []models.Transaction{}
	operation := models.OperationType{}
	err := db.DB.Table("operation_types").Where("description = ? ", "PAGAMENTO").First(&operation).Error
	if err != nil{
		return retorno, err
	}
	parcelada := models.OperationType{}
	err = db.DB.Table("operation_types").Where("description = ? ", "COMPRA PARCELADA").First(&parcelada).Error
	if err != nil{
		return retorno, err
	}
	//Mapa de cliente por transações, para não refazer a busca se tiverem multiplos pagamentos do mesmo cliente
	//Mapas com os valores a serem restaurados as contas dos clientes, armazenando em cache para fazer a operação apenas uma vez por cliente
	var transactions = map[int64][]models.Transaction{}
	var creditLimit = map[int64]float64{}
	var withdrawLimit = map[int64]float64{}

	for _, pay := range payments{
		idAccount := pay.AccountId
		balance := pay.Amount

		if _, ok:= transactions[idAccount]; !ok{
			var err error
			transactions[idAccount], err = getTransactions(idAccount, w)
			if err != nil{
				return retorno, err
			}
		}

		for i := 0 ; i < len(transactions[idAccount]) && balance > 0.0 ; i++{
			//transaction := transactions[idAccount][i]

			minimum := math.Min(balance, transactions[idAccount][i].Balance * -1)

			if minimum != 0.0 {
				balance -= minimum
				transactions[idAccount][i].Balance += minimum
				db.DB.Save(&transactions[idAccount][i])

				if transactions[idAccount][i].OperationTypeId == int64(parcelada.ID){
					creditLimit[idAccount] += minimum
				}else{
					withdrawLimit[idAccount] += minimum
				}
			}
		}
		/*FLUXO
		- Se tiver saldo negativo, abater seguindo a RN (ordem por operação e data)
			- Tudo que for abatido vai ser restaurado na conta do cliente AvailableCreditLimit e AvailableWithdrawalLimit
		- Se sobrar dai fica no balance da transação de pagamento
		- Persistir a transação
		 */
		transacao := models.Transaction{Amount:pay.Amount, Balance: balance, DueDate:time.Now(),
			EventDate:time.Now(), OperationTypeId:int64(operation.ID), AccountId:pay.AccountId}

		db.DB.Save(&transacao)

		retorno = append(retorno, transacao)
	}

	//atualizar creditos dos clientes
	for key, val := range creditLimit{
		db.DB.Exec("UPDATE accounts SET available_credit_limit = available_credit_limit + ? WHERE id = ? ", val, key)
	}

	//atualizar limites de saques dos clientes
	for key, val := range withdrawLimit{
		db.DB.Exec("UPDATE accounts SET available_withdrawal_limit = available_withdrawal_limit + ? WHERE id = ? ", val, key)
	}

	return retorno, nil
}

//Busca as transações negativas de um dado cliente, já obdecendo a RN sobre a ordem que são abatidas
//Ordenar pelo menor "charge_order" das operações e depois pelo "event_date" da transação
func getTransactions(idAccount int64, w http.ResponseWriter) ([]models.Transaction, error){
	var transations []models.Transaction

	err := db.DB.Table("transactions").Select("transactions.id, transactions.account_id, transactions.balance, " +
		"transactions.operation_type_id, transactions.amount, transactions.event_date, transactions.due_date").
		Joins("INNER JOIN operation_types ON operation_types.id = transactions.operation_type_id").
		Where("transactions.account_id = ? AND transactions.balance < 0  AND operation_types.description IN (?)", idAccount, debitTypes).
		Order("operation_types.charge_order ASC, transactions.event_date ASC").Find(&transations).Error
	if err != nil {
		fmt.Println(err)
		utils.Respond(w, utils.Message(false, err.Error()))
		return transations, err
	}

	return transations, nil
}