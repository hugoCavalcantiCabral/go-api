# go-api

##Configurações de ambiente
```
Ubuntu
PostgreSQL 10.6
go1.10.3 linux/amd64
```

## Bibliotecas externas para instalar:

gorilla/mux — Tratamento das rotas e match com os controllers.

jinzhu/gorm — Biblioteca ORM(Object Relational Mapper) para interagir com a base de dados.

dgrijalva/jwt-go — Biblioteca para autenticação de tokens JWT.

joho/godotenv — Usado para carregar o .env no projeto.

lib/pq — PostgreSQL driver.

brianvoe/gofakeit - Gerador de massa de teste

**OBS:** Para instalar as bibliotecas, e conseguir executar o projeto, basta rodar o comando `go get github.com/{nome-do-pacote}`
 
----------------------------------  

## Rodar a API

Foram usadas 2 base de dados postgres, uma para testes e outra para a aplicação, sendo assim é necessário executar o seguinte sql para criação das bases:
```sql
CREATE DATABASE teste;

CREATE DATABASE development;
```
As configurações de banco se encontram em `.env` se quiser modificar 

Para rodar a API executar o comando `go run main.go` na pasta raiz do projeto

## Testes

Todos os testes se encontram no pacote `tests`

Para rodar os testes executar `go test ./...` na pasta raiz do projeto

Na linguagem GO os arquivos de testes seguem o padrão `*_test.go`

##TODO
- Adicionar a autenticação
- Adicionar validação sobre as estruturas dos json recebidos
- Utilizar Docker 

##Anotações 
- Ao tentar sacar ou utilizar o crédito no valor maior que o disponivel na conta será exibido a mensagem de erro "Reject", segui esse padrão pq acho que é o padrão usado nas máquinas de crédito, para não denegrir a imagem do usuário com uma mensagem mais clara
- Não sei o que fazer com a coluna due_date das transações

##Endpoints

- GET   /v1/accounts/limits
    * Busca todas as contas cadastradas
    * Retorno:
```
{
     "accounts": [
         {
             "ID": 1,
             "CreatedAt": "2019-04-28T22:24:40.489403-03:00",
             "UpdatedAt": "2019-04-28T22:24:40.489403-03:00",
             "DeletedAt": null,
             "available_credit_limit": 100,
             "available_withdrawal_limit": 50
         },
         {
             "ID": 2,
             "CreatedAt": "2019-04-28T22:24:43.884663-03:00",
             "UpdatedAt": "2019-04-28T22:24:43.884663-03:00",
             "DeletedAt": null,
             "available_credit_limit": 10,
             "available_withdrawal_limit": 5
         }
     ],
     "message": "success",
     "status": true
 }
```

- GET   /v1/accounts/limits/{id}
    * Busca uma conta pelo id
    * Retorno:
```
{
    "accounts": {
        "ID": 1,
        "CreatedAt": "2019-04-28T22:24:40.489403-03:00",
        "UpdatedAt": "2019-04-28T22:24:40.489403-03:00",
        "DeletedAt": null,
        "available_credit_limit": 100,
        "available_withdrawal_limit": 50
    },
    "message": "success",
    "status": true
}
```

- PATCH /v1/accounts/{id}
    * Modifica os limites de saque e credito de um cliente
        * **OBS:** Não permite modificar para valores negativos
    * Entrada/Retorno:
```
{
	"available_credit_limit": {
		"amount": 45
	},
	"available_withdrawal_limit": {
		"amount": 20
	}
}
```
```
{
    "accounts": {
        "ID": 1,
        "CreatedAt": "2019-04-28T22:24:40.489403-03:00",
        "UpdatedAt": "2019-04-28T22:34:03.037438564-03:00",
        "DeletedAt": null,
        "available_credit_limit": 145,
        "available_withdrawal_limit": 70
    },
    "message": "success",
    "status": true
}
```

- POST  /v1/transactions
    * Gera transações dos tipos "COMPRA A VISTA", "COMPRA PARCELADA", "SAQUE"
        * **OBS:** Apenas a "COMPRA PARCELADA" consome do "available_credit_limit" do cliente, as outras 2 operações consomem de "available_withdrawal_limit"
        * **OBS:** Não são permitidos valores positivos nos amounts enviados aqui, apenas para melhorar o entendimento
    * Entrada/Retorno:  
```
{
	"account_id": 1,
	"operation_type_id": 2,
	"amount": -10
}
```
```
{
    "message": "success",
    "status": true,
    "transaction": {
        "ID": 1,
        "CreatedAt": "2019-04-28T22:38:46.502994966-03:00",
        "UpdatedAt": "2019-04-28T22:38:46.502994966-03:00",
        "DeletedAt": null,
        "account_id": 1,
        "operation_type_id": 2,
        "amount": -10,
        "balance": -10,
        "event_date": "2019-04-28T22:38:46.502900221-03:00",
        "due_date": "2019-04-28T22:38:46.502900045-03:00"
    }
}
```

- POST  /v1/payments
    * Gera as transações do tipo "PAGAMENTO" dos cliente, podem ser enviados mais de uma por vez
        * **OBS:** Não são permitidos valores negativos para os amounts aqui, para também melhorar o entendimento
```
[
	{
		"account_id": 1,
		"amount": 10
	}
]
```
```
{
    "message": "success",
    "payment": [
        {
            "ID": 2,
            "CreatedAt": "2019-04-28T22:42:23.477201375-03:00",
            "UpdatedAt": "2019-04-28T22:42:23.477201375-03:00",
            "DeletedAt": null,
            "account_id": 1,
            "operation_type_id": 4,
            "amount": 10,
            "balance": 0,
            "event_date": "2019-04-28T22:42:23.476990288-03:00",
            "due_date": "2019-04-28T22:42:23.476989958-03:00"
        }
    ],
    "status": true
}
```