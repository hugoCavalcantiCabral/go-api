package models

type Payment struct {
	AccountId int64   `json:"account_id"`
	Amount    float64 `json:"amount"`
}
