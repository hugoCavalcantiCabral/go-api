package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Transaction struct {
	gorm.Model
	AccountId int64 `gorm:"type:bigint REFERENCES accounts(id)" json:"account_id"`
	//OperationType OperationType `gorm:"foreignkey:OperationTypeId"`
	OperationTypeId int64 `json:"operation_type_id"` // gorm:"type:bigint REFERENCES operation_types(id)"`
	Amount float64 `json:"amount"`
	Balance float64 `json:"balance"`
	EventDate time.Time `json:"event_date"`
	DueDate time.Time `json:"due_date"`
}
