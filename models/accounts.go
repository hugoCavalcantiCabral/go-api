package models

import (
	"github.com/jinzhu/gorm"
)

type Account struct {
	gorm.Model
	AvailableCreditLimit float64 `json:"available_credit_limit"`
	AvailableWithdrawalLimit float64 `json:"available_withdrawal_limit"`
}
